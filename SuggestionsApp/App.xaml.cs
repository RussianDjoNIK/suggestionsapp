﻿using System;
using System.Collections.Generic;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Caliburn.Micro;
using Suggestions.Common;
using SuggestionsApp.Services;
using SuggestionsApp.ViewModels;

namespace SuggestionsApp
{
    public sealed partial class App
    {
        private WinRTContainer _container;

        public App()
        {
            InitializeComponent();
        }

        protected override void Configure()
        {
            // We will use x:Bind and they don't work together. So turn off conventions
            ViewModelBinder.ApplyConventionsByDefault = false;

            _container = new WinRTContainer();
            _container.RegisterWinRTServices();

            RegisterCustomServices();

            RegisterViewModels();
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void OnSuspending(object sender, SuspendingEventArgs e)
        {
            // TODO suspending advice (MessageBox?)
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        private void RegisterCustomServices()
        {
            _container.Singleton<IDelayProvider, DelayProviderService>();

            var dispatcher = new UwpDispatcher(Window.Current.Dispatcher);
            _container.Instance<IDispatcher>(dispatcher);
        }

        private void RegisterViewModels()
        {
            _container.PerRequest<ShellViewModel>();
        }
    }
}