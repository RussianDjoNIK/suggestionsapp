﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using Suggestions.Common;
using SuggestionsApp.Services.Suggestions;

namespace SuggestionsApp.ViewModels
{
    public class ShellViewModel : Screen
    {
        #region Fields

        private static readonly object SyncObj = new object();

        private readonly IDelayProvider _delayProvider;
        private readonly IDispatcher _dispatcher;
        private readonly IList<string> _fullCollection;
        private BindableCollection<string> _suggestions;
        private string _searchingText;

        #endregion

        #region Properties

        public string SearchingText
        {
            get { return _searchingText ?? string.Empty; }
            set
            {
                _searchingText = value;
                NotifyOfPropertyChange();

                UpdateSuggestions();
            }
        }

        public IEnumerable<string> Suggestions
        {
            get { return _suggestions; }
            private set
            {
                _suggestions = new BindableCollection<string>(value);

                NotifyOfPropertyChange();
            }
        }

        #endregion

        #region Constructor

        public ShellViewModel(IDelayProvider delayProvider, IDispatcher dispatcher)
        {
            Debug.Assert(null != delayProvider);
            Debug.Assert(null != dispatcher);

            _delayProvider = delayProvider;
            _dispatcher = dispatcher;

            _fullCollection = new List<string>();

            Suggestions = new BindableCollection<string>();
        }

        #endregion

        #region Public Methods

        // TODO ToLower? What if add option case sensitive?
        // TODO OrderBy? Ordering by Descending
        // TODO Contains? StartWith etc.
        public void UpdateSuggestions()
        {
            // ReSharper disable once InconsistentlySynchronizedField
            var res = _fullCollection.Where(str => str.ToLower().Contains(SearchingText.ToLower())).OrderBy(s => s);
                // Calling from dispatcher
            Suggestions = new BindableCollection<string>(res);
        }

        #endregion

        protected override async void OnActivate()
        {
            await StartCollectData();
        }

        private async Task StartCollectData()
        {
            var providers = new List<SuggestionProvider>
            {
                new DiskSuggestionProvider(_delayProvider),
                new NetworkSuggestionProvider(_delayProvider),
                new FakeSuggestionProvider(_delayProvider),
                new RandomSuggestionProvider(_delayProvider),
                new HamletSuggestionProvider(_delayProvider)
            };

            foreach (var suggestionProvider in providers)
            {
                try
                {
                    var res = await CancelableStringGetter(suggestionProvider, 1000);
                    _dispatcher.RunAsync(() =>
                    {
                        lock (SyncObj)
                        {
                            foreach (var str in res)
                            {
                                _fullCollection.Add(str);
                            }

                            UpdateSuggestions();
                        }
                    });
                }
                catch (TaskCanceledException ex)
                {
                    // TODO log this message
                    Debug.WriteLine("Suggestion {0}. Exception \"{1}\".", suggestionProvider.Name, ex.Message);
                }
            }
        }

        private Task<IEnumerable<string>> CancelableStringGetter(SuggestionProvider provider, int timeoutMs)
        {
            var tcs = new TaskCompletionSource<IEnumerable<string>>();
            var ct = new CancellationTokenSource(timeoutMs);
            ct.Token.Register(() =>
                    tcs.TrySetCanceled(), false);

            var awaiter =  provider.GetAsync().ConfigureAwait(false).GetAwaiter();
            awaiter.OnCompleted(() =>
            {
                var res = awaiter.GetResult();
                tcs.TrySetResult(res);
            });
            

            return tcs.Task;
        }
    }
}