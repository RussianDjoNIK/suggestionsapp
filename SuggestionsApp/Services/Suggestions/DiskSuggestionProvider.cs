﻿using System.Collections.Generic;
using Suggestions.Common;

namespace SuggestionsApp.Services.Suggestions
{
    internal class DiskSuggestionProvider : SuggestionProviderWrapper
    {
        #region Fields

        private readonly List<string> _data = new List<string>
        {
            "po1sfCcXZS",
            "0zgX1vk6ei",
            "ObJhWmxfY0",
            "lXEcUgqMSR",
            "mBSXic6LPZ",
            "9my8zN5X4Y",
            "Qj4nMZrmMp",
            "dZmOSYg82P",
            "VJfZl4s1gL",
            "uAnS4AtW82",
            "sxLarJGqCS",
            "oBlUoPu61M",
            "Y1KVdDOY3r",
            "f2fAH4jIkz",
            "ZEIOC7PY60",
            "7dWGep3JeW",
            "WeiXWyYK0V",
            "fQnivbYVwG",
            "6Li4z3SL1r",
            "vpArn2vnqu",
            "cOXij0cQ13",
            "SyBejUWdno",
            "9igGHwCcsl",
            "aeQp1tbWlH",
            "HlC6OiqbW2",
            "1lQuc6VDtb",
            "5j6cryCYEw",
            "u9OJr35Fyh",
            "00SErybWYT",
            "bwiDa9UEgJ",
            "qLF6N5rdJA",
            "Oa6MNVqQJ5",
            "i9nyDyrCbT",
            "xdpxJwPzXk",
            "vZ3Vl7RURz",
            "M9cDwCxRmr",
            "YsOZk3dVlO",
            "DykQi28IuW",
            "Bc7Z6z5v1A",
            "31bN1rT7UU",
            "YEwz4YqRFt",
            "qwUtIYOjB8",
            "5g4CMyZbbB",
            "PR7qQicU8t",
            "pUyWe5mRyF",
            "rtLUHN0jxF",
            "gkWtJ3z1Xk",
            "l9pDhJcBFT",
            "u48F4xtBL9",
            "Zj9iP8gsPH"
        };

        #endregion

        #region Properties

        public override string Name => "Disk";

        protected override List<string> Data => _data;

        #endregion

        #region Constructor

        public DiskSuggestionProvider(IDelayProvider delayProvider) : base(delayProvider)
        {
        }
        
        #endregion
    }
}
