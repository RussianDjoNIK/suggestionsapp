﻿using System.Collections.Generic;
using Suggestions.Common;

namespace SuggestionsApp.Services.Suggestions
{
    internal class FakeSuggestionProvider : SuggestionProviderWrapper
    {
        #region Fields

        private readonly List<string> _data = new List<string>
        {
            "Lorem",
            "ipsum",
            "dolor",
            "sit",
            "amet,",
            "consectetuer",
            "adipiscing",
            "elit.",
            "Aenean",
            "commodo",
            "ligula",
            "eget",
            "dolor.",
            "Aenean",
            "massa.",
            "Cum",
            "sociis",
            "natoque",
            "penatibus",
            "et",
            "magnis",
            "dis",
            "parturient",
            "montes,",
            "nascetur",
            "ridiculus",
            "mus.",
            "Donec",
            "quam",
            "felis"
        };

        #endregion

        #region Properties

        public override string Name => "Fake";

        protected override List<string> Data => _data;

        #endregion

        #region Constructor

        public FakeSuggestionProvider(IDelayProvider delayProvider) : base(delayProvider)
        {
        }
        
        #endregion
    }
}
