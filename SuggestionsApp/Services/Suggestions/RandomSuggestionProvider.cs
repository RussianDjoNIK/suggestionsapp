﻿using System.Collections.Generic;
using Suggestions.Common;

namespace SuggestionsApp.Services.Suggestions
{
    internal class RandomSuggestionProvider : SuggestionProviderWrapper
    {
        #region Fields

        private readonly List<string> _data = new List<string>
        {
            "ApfSQwotVPmVQGLbnvDS",
            "oDuuOAQDOOUdiqEIuFMx",
            "teiusKlUohXLPPUWCMMx",
            "EoMtPVXcZxkWFDuRAfsX",
            "iVTeoXgSeeccfWmoVbON",
            "jfKKxvKOPaXeoxjEetdn",
            "xJjbXslThLqOejGedgeR",
            "AevJzjqVUKqySpFIUqgQ",
            "AEFPdEDSIwKnkrBcDHtM",
            "iHuFClxoajwgeuwSPvdi",
            "lazfPdMIxzxzEihwFhJr",
            "jLhYfvlxKyFMiqMujQJA",
            "VHKvvshBvXXaqrWvBwXA",
            "xcGmHQPalojoQNcAVSfC",
            "jHnmUbQBDxBAJNMZZEFi"
        };

        #endregion

        #region Properties

        public override string Name => "Random";

        protected override List<string> Data => _data;

        #endregion

        #region Constructor

        public RandomSuggestionProvider(IDelayProvider delayProvider) : base(delayProvider)
        {
        }
        
        #endregion
    }
}
