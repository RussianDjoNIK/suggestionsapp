﻿using System.Collections.Generic;
using Suggestions.Common;

namespace SuggestionsApp.Services.Suggestions
{
    internal abstract class SuggestionProviderWrapper : SuggestionProvider
    {
        #region Fields

        private readonly IDelayProvider _delayProvider;
        protected virtual List<string> Data { get; }

        #endregion

        #region Constructor

        protected SuggestionProviderWrapper(IDelayProvider delayProvider) : base(delayProvider)
        {
            _delayProvider = delayProvider;
            Data = new List<string>();
        }

        #endregion

        #region

        protected override IEnumerable<string> GetImplementation()
        {
            foreach (var pieceOfData in Data)
            {
                yield return pieceOfData;
            }
        }

        protected override int GetDelay()
        {
            return _delayProvider.GetMillisecondsDelay();
        }

        #endregion
    }
}
