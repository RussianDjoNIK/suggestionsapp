﻿using System.Collections.Generic;
using Suggestions.Common;

namespace SuggestionsApp.Services.Suggestions
{
    internal class NetworkSuggestionProvider : SuggestionProviderWrapper
    {
        #region Fields

        private readonly List<string> _data = new List<string>
        {
            "Imperdiet",
            "a",
            "eu",
            "dui",
            "pellentesque",
            "natoque",
            "faucibus",
            "iaculis",
            "pellentesque.",
            "Quisque",
            "cubilia.",
            "Elit",
            "eu",
            "iaculis",
            "semper",
            "est",
            "ultricies",
            "mi.",
            "Class",
            "vitae",
            "integer",
            "lacus",
            "volutpat",
            "turpis.",
            "Quis,",
            "tempor.",
            "Vehicula",
            "eu",
            "pretium",
            "feugiat",
            "torquent",
            "fames",
            "viverra",
            "dictumst",
            "nunc",
            "parturient",
            "non",
            "dictum",
            "pellentesque",
            "rhoncus"
        };

        #endregion

        #region Properties

        public override string Name => "Network";

        protected override List<string> Data => _data;

        #endregion

        #region Constructor

        public NetworkSuggestionProvider(IDelayProvider delayProvider) : base(delayProvider)
        {
        }
        
        #endregion
    }
}
