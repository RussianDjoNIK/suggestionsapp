﻿using System;
using Suggestions.Common;

namespace SuggestionsApp.Services
{
    internal class DelayProviderService : IDelayProvider
    {
        private const int MinDelay = 300;
        private const int MaxDelay = 2000;

        private readonly Random _randomizer = new Random();

        public int GetMillisecondsDelay()
        {
            var res = _randomizer.Next(MinDelay, MaxDelay);
            return res;
        }
    }
}
