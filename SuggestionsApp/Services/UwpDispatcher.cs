﻿using System;
using Windows.UI.Core;
using Suggestions.Common;

namespace SuggestionsApp.Services
{
    public class UwpDispatcher : IDispatcher
    {
        private readonly CoreDispatcher _coreDispatcher;

        public UwpDispatcher(CoreDispatcher coreDispatcher)
        {
            _coreDispatcher = coreDispatcher;
        }

        public async void RunAsync(Action uiAction)
        {
            await _coreDispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                uiAction();
            });
        }
    }
}
