﻿using System.Diagnostics;
using Windows.UI.Xaml;
using SuggestionsApp.ViewModels;

namespace SuggestionsApp.Views
{
    public sealed partial class ShellView
    {
        #region Properties

        public ShellViewModel ViewModel { get; private set; }

        #endregion
        
        #region Constructor
        
        public ShellView()
        {
            InitializeComponent();

            // UWP x:Bind workaround
            DataContextChanged += OnDataContextChanged;
        }

        #endregion

        #region

        private void OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            Debug.Assert(null != args.NewValue, "Context shouldn't be empty");
            var vm = args.NewValue as ShellViewModel;
            Debug.Assert(null != vm, "Context should be ShellViewModel");

            ViewModel = vm;
        }

        #endregion
    }
}
