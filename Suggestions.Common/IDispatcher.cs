﻿using System;

namespace Suggestions.Common
{
    public interface IDispatcher
    {
        void RunAsync(Action uiAction);
    }
}