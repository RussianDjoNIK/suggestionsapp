using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Suggestions.Common
{
    public abstract class SuggestionProvider
    {
        #region Fields


        private readonly IDelayProvider _delayProvider;

        #endregion

        #region Properties

        public abstract string Name { get; }

        #endregion

        #region Constructor

        protected SuggestionProvider(IDelayProvider delayProvider)
        {
            Debug.Assert(null != delayProvider, "DelayProvider shouldn't be null");

            _delayProvider = delayProvider;
        }

        #endregion

        #region Public Methods

        public async Task<IEnumerable<string>> GetAsync()
        {
            var delay = GetDelay();
            await Task.Delay(delay);

            return GetImplementation();
        }

        #endregion

        #region Private and Protected Methods

        protected abstract IEnumerable<string> GetImplementation();

        protected virtual int GetDelay()
        {
            return _delayProvider.GetMillisecondsDelay();
        }

        #endregion
    }
}
