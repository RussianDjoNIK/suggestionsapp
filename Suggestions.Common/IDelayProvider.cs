namespace Suggestions.Common
{
    public interface IDelayProvider
    {
        int GetMillisecondsDelay();
    }
}
